import java.util.*;

/**
 * Created by Евгения on 06.09.14.
 */
public class Predicate extends Expression {

    String name;
    protected List<Term> args;


    public Predicate(String name, List<Term> args) {
        this.name = name;
        this.args = (args == null) ? null : new ArrayList<>(args);
    }

    @Override
    protected Expression substituteImpl(HashMap<Predicate, Expression> substByVarName) {
        return substByVarName.containsKey(this) ? substByVarName.get(this) : null;
    }

    @Override
    protected boolean isOneVariableTemplateImpl(ObjectVariable replaceWhat, Expression with,
                                                SubstitutionWrapper sw, HashSet<ObjectVariable> bound) {
        if (getClass() != with.getClass() || args != null && args.size() != ((Predicate) with).args.size() ||
                !name.equals(((Predicate) with).name))
            return false;
        if (args != null)
            for (int i = 0; i < args.size(); i++)
                    if (!args.get(i).equalsWithObjectVariableSubstitution(replaceWhat, ((Predicate) with).args.get(i),
                            sw, bound))
                        return false;
        return true;
    }

    @Override
    protected boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables, Set<ObjectVariable> bound) {
        if (args!=null)
            for (Term arg : args)
                if (!arg.freeForSubstitutionImpl( x, freeVariables, bound))
                    return false;
        return true;
    }

    @Override
    protected void getFreeVariablesImpl(Set<ObjectVariable> allFreeVariables) {
        if (args!=null)
            for (int i=0; i<args.size();i++)
                args.get(i).getFreeVariables(allFreeVariables);
    }

    @Override
    protected boolean isTemplateImpl(Expression formToCompare, HashMap<String, Expression> variable) {
        if (!variable.containsKey(name))
            variable.put(name, formToCompare);
        else
            return (formToCompare.equals(variable.get(name)));
        return true;
    }

    @Override
    protected void getPropositionalVariablesImpl(TreeSet<Predicate> set) {
        if (!set.contains(this))
            set.add(this);
    }


    @Override
    protected String getStringRepresentation() {
        String argsStrings = "";
        if (args != null) {
            for (int i = 0; i < args.size(); i++) {
                Term term = args.get(i);
                argsStrings += term;
                if (i != args.size() - 1)
                    argsStrings += ", ";
            }
            return name + "(" + argsStrings + ")";
        } else
            return name;
    }
}

class EqualsPredicate extends Predicate {

    public EqualsPredicate(Term left, Term right) {
        super("=", Arrays.asList(left, right));
    }

    @Override
    protected String getStringRepresentation() {
        return args.get(0).toString() + name + args.get(1).toString();
    }
}
