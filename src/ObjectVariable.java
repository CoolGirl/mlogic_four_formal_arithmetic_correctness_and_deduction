import java.util.HashSet;
import java.util.Set;

/**
 * Created by Евгения on 16.02.2015.
 */
public class ObjectVariable extends Term {

    public ObjectVariable(String name) {
        super(name);
    }

    @Override
    protected void getFreeVariables(Set<ObjectVariable> allFreeVariables) {
        if (!allFreeVariables.contains(this))
            allFreeVariables.add(this);
    }

    @Override
    protected boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables,
                                              Set<ObjectVariable> bound) {
        if (!x.equals(this) || bound.contains(this))
            return true;
        for (ObjectVariable freeVariable : freeVariables)
            if (bound.contains(freeVariable))
                return false;
        return true;
    }


    @Override
    public boolean equalsWithObjectVariableSubstitution(ObjectVariable replaceWhat, Term with,
                                                        Expression.SubstitutionWrapper sw,
                                                        HashSet<ObjectVariable> bound) {
        return matchesForSubstitution(replaceWhat, with, sw, bound);
    }

    private boolean matchesForSubstitution(ObjectVariable replaceWhat, Term with, Expression.SubstitutionWrapper
            sw, HashSet<ObjectVariable> bound) {
        if (bound.contains(this)) {
            if (!equals(with))
                return false;
        } else { // !bound.contains(this)
            if (this.equals(replaceWhat)) {
                if (sw.substitution != null)
                    return with.equals(sw.substitution);
                else {
                    sw.substitution = with;
                }
                return true;
            } else {
                return equals(with);
            }
        }
        return true;
    }

    @Override
    protected String getStringRepresentation() {
        return name;
    }
}
