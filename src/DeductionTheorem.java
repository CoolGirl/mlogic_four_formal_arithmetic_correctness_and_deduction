import java.util.*;

/**
 * Created by Евгения on 11.02.2015.
 */
public class DeductionTheorem {

    //It requires removing last used assumption
    public static ArrayList<Expression> apply(List<? extends Expression> assumptions, ArrayList<Expression> betaProof) throws ProofException {
        ArrayList<Expression> proof = new ArrayList<>();
        assumptions = assumptions.subList(0, assumptions.size());
        Set<ObjectVariable> forbiddenVariables = assumptions.get(assumptions.size()-1).getFreeVariables();
        Verifier verifier = new Verifier();
        HashMap<Predicate,Expression> substForAlphaImplAlpha = new HashMap<>();
        substForAlphaImplAlpha.put(new Predicate("A", null), assumptions.get(assumptions.size() - 1));
        for (int i = 0; i < betaProof.size(); i++) {
            VerifyingResult verifyingResult = verifier.verifyExpression(betaProof.get(i), i + 1);
            if (verifyingResult.isModusPonens()) {
                HashMap<Predicate, Expression> substMap = new HashMap<>();
                substMap.put(new Predicate("A", null), assumptions.get(assumptions.size()-1));
                substMap.put(new Predicate("B", null),   betaProof.get(verifyingResult.getAxiomOrFirstPonensArg() - 1));
                substMap.put(new Predicate("C", null),
                        ((BinaryOperation) betaProof.get((verifyingResult.getSecondPonensArg()) - 1)).right);
                proof.addAll(Expression.substituteAll(outputIfModusPonens, substMap));
            } else if (betaProof.get(i).equals(assumptions.get(assumptions.size()-1)))
                proof.addAll(Expression.substituteAll(aImplA,substForAlphaImplAlpha));
            else if (assumptions.contains(betaProof.get(i)) || Verifier.isAxiomOrAxiomScheme(betaProof.get(i))) {
                proof.add(betaProof.get(i));
                HashMap<Predicate, Expression> substMap = new HashMap<>();
                substMap.put(new Predicate("A", null), betaProof.get(i));
                substMap.put(new Predicate("B", null), assumptions.get(assumptions.size()-1));
                Expression secondExpression = Verifier.axiomSchemes.get(0).substitute(substMap);
                proof.add(secondExpression);
                proof.add(((BinaryOperation) secondExpression).right);
            }
            else if (((Implication) betaProof.get(i)).right instanceof UniversalQuantifier)
                if(!forbiddenVariables.contains(
                    ((UniversalQuantifier) ((Implication) betaProof.get(i)).right).getObjectVariable()
            )){
                HashMap<Predicate,Expression> substMap = new HashMap<>();
                substMap.put(new Predicate("A",null), assumptions.get(assumptions.size()-1));
                substMap.put(new Predicate("B",null), ((Implication) betaProof.get(i)).left);
                substMap.put(new Predicate("C",null), ((UniversalQuantifier) ((Implication) betaProof.get(i)).right).getExpression());
                substMap.put(new Predicate("P",null), ((Implication) betaProof.get(i)).right);
                proof.addAll(Expression.substituteAll(outputIfUniversalQuantifier, substMap));
            }
                else
                    throw new ProofException("используется правило с квантором по переменной " +
                            ((UniversalQuantifier) ((Implication) betaProof.get(i)).right).getObjectVariable() +
                            ", входящей свободно в допущение " +
                            assumptions.get(assumptions.size()-1));
            else if (((Implication) betaProof.get(i)).left instanceof ExistentialQuantifier)
                if  (!forbiddenVariables.contains(((Implication) betaProof.get(i)).left)){
                HashMap<Predicate,Expression> substMap = new HashMap<>();
                substMap.put(new Predicate("A",null), assumptions.get(assumptions.size()-1));
                substMap.put(new Predicate("B",null), ((ExistentialQuantifier) ((Implication) betaProof.get(i)).left).getExpression());
                substMap.put(new Predicate("C",null), ((Implication) betaProof.get(i)).right);
                substMap.put(new Predicate("E",null), ((Implication) betaProof.get(i)).left);
                proof.addAll(Expression.substituteAll(outputIfExistentialQuantifier, substMap));
            }
            else
                    throw new ProofException("используется правило с квантором по переменной " +
                            ((ExistentialQuantifier) ((Implication) betaProof.get(i)).right).getObjectVariable() +
                            ", входящей свободно в допущение " +
                            assumptions.get(assumptions.size()-1));
        }
        return proof;
    }

    public static final List<Expression> aImplA;
    public static final List<Expression> outputIfModusPonens;
    public static final List<Expression> outputIfUniversalQuantifier;
    public static final List<Expression> outputIfExistentialQuantifier;

    static {
        aImplA = Expression.parseFile("A_impl_A.txt");
        outputIfModusPonens = Expression.parseFile("outputIfModusPonens.txt");
        outputIfUniversalQuantifier = Expression.parseFile("deductionTransformationForUniversalQuantifier");
        outputIfExistentialQuantifier = Expression.parseFile("deductionTransformationForExistentialQuantifier");
    }

}
