import java.util.HashMap;

/**
 * Created by Евгения on 06.09.14.
 */
public class Implication extends BinaryOperation {


    public Implication(Expression parsedExpression, Expression expression) {
        super(parsedExpression, expression);
        sign=getSign();
    }

    @Override
    protected String getStringRepresentation() {
        boolean leftBrackets = false, rightBrackets = false;
        if (left instanceof BinaryOperation && isThisPriorityHigher(sign, ((BinaryOperation) left).getSign())
                ||(left instanceof Implication))
            leftBrackets = true;
        if (right instanceof BinaryOperation && isThisPriorityHigher(sign, ((BinaryOperation) right).getSign()))
            rightBrackets = true;
        return ((leftBrackets ? "(" : "")
                + left.toString()
                + (leftBrackets ? ")" : "")
                + sign
                + (rightBrackets ? "(" : "")
                + right.toString()
                + (rightBrackets ? ")" : ""));
    }

    @Override
    protected Expression substituteImpl(HashMap<Predicate, Expression> substByVarName) {
        Expression leftSubstitute = left.substituteImpl(substByVarName);
        if (leftSubstitute != null)
            return new Implication(leftSubstitute, right.substitute(substByVarName));
        Expression rightSubstitute = right.substituteImpl(substByVarName);
        if (rightSubstitute != null)
            return new Implication(left, rightSubstitute);
        return null;
    }

    @Override
    public String getSign() {
        return "->";
    }
}
