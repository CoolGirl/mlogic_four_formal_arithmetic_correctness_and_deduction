import java.util.*;

/**
 * Created by Евгения on 16.02.2015.
 */
public class ObjectFunction extends Term {

    protected List<Term> args;

    @Override
    protected void getFreeVariables(Set<ObjectVariable> allFreeVariables) {
        if (args != null)
            for (Term arg : args) arg.getFreeVariables(allFreeVariables);
    }

    @Override
    protected boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables,
                                              Set<ObjectVariable> bound) {
        if (args != null)
            for (Term arg : args)
                if (!arg.freeForSubstitutionImpl(x, freeVariables, bound))
                    return false;
        return true;
    }

    @Override
    public boolean equalsWithObjectVariableSubstitution(ObjectVariable replaceWhat, Term with,
                                                        Expression.SubstitutionWrapper sw,
                                                        HashSet<ObjectVariable> bound) {
        if (getClass() != with.getClass() || args != null && args.size() != ((ObjectFunction) with).args.size() ||
                !name.equals(((ObjectFunction) with).name))
            return false;
        if (args != null)
            for (int i = 0; i < args.size(); i++)
                if (!args.get(i).equalsWithObjectVariableSubstitution(replaceWhat, ((ObjectFunction) with).args.get(i)
                        , sw, bound))
                    return false;
        return true;
    }

    @Override
    protected String getStringRepresentation() {
        String argsStrings = "";
        if (args != null)
            for (int i = 0; i < args.size(); i++) {
                Term term = args.get(i);
                argsStrings += term;
                if (i != args.size() - 1)
                    argsStrings += ", ";
            }
        return name + "(" + argsStrings + ")";
    }

    public ObjectFunction(String name, List<Term> args) {
        super(name);
        if (args != null)
            this.args = new ArrayList<>(args);
    }

    public static Zero ZERO = Zero.INSTANCE;
}

class Addition extends BinaryObjectFunction {
    public Addition(Term left, Term right) {
        super("+", left, right);
    }
}

class BinaryObjectFunction extends ObjectFunction {

    public BinaryObjectFunction(String name, Term left, Term right) {
        super(name, Arrays.asList(left, right));
    }

    @Override
    public String getStringRepresentation() {
        String arg1 = args.get(0).toString();
        String arg2 = args.get(1).toString();
        if (args.get(0) instanceof Addition || args.get(0) instanceof Multiplication)
            arg1 = "(" + arg1 + ")";
        if (args.get(1) instanceof Addition || args.get(1) instanceof Multiplication)
            arg2 = "(" + arg2 + ")";
        return arg1 + name + arg2;
    }

}

class Multiplication extends BinaryObjectFunction {
    public Multiplication(Term left, Term right) {
        super("*", left, right);
    }
}

class Zero extends ObjectFunction {

    @Override
    public String getStringRepresentation() {
        return "0";
    }

    private Zero() {
        super("0", new ArrayList<Term>());
    }

    public static Zero INSTANCE = new Zero();
}

class Increment extends ObjectFunction {

    public Increment(Term arg) {
        super("'", Arrays.asList(arg));
    }

    @Override
    public String getStringRepresentation() {
        return "("+args.get(0).toString()+")" + name;
    }
}