/**
 * Created by Евгения on 01.03.2015.
 */
public class ProofException  extends Exception{

    public ProofException(String msg){
        super(msg);
    }
}
