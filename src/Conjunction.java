import java.util.HashMap;

/**
 * Created by Евгения on 06.09.14.
 */
public class Conjunction extends BinaryOperation {


    public Conjunction(Expression parsedExpression, Expression expression) {
        super(parsedExpression, expression);
        sign = getSign();
    }

    @Override
    protected Expression substituteImpl(HashMap<Predicate, Expression> substByVarName) {
        Expression leftSubstitute = left.substituteImpl(substByVarName);
        if (leftSubstitute != null)
            return new Conjunction(leftSubstitute, right.substitute(substByVarName));
        Expression rightSubstitute = right.substituteImpl(substByVarName);
        if (rightSubstitute != null)
            return new Conjunction(left, rightSubstitute);
        return null;
    }

    @Override
    public String getSign() {
        return "&";
    }
}
