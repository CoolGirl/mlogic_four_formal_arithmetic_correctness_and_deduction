import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Евгения on 07.09.14.
 */
public class Negation extends Expression {

    private Expression operand;
    public static final String sign = "!";


    public Negation(Expression operand) {
        this.operand = operand;
    }

    @Override
    protected Expression substituteImpl(HashMap<Predicate, Expression> substByVarName) {
        return new Negation(operand.substitute(substByVarName));
    }

    @Override
    protected boolean isOneVariableTemplateImpl(ObjectVariable replaceWhat, Expression with,
                                                SubstitutionWrapper sw, HashSet<ObjectVariable> bound) {
        return getClass() == with.getClass() && operand.isOneVariableTemplateImpl(replaceWhat,
                ((Negation) with).operand, sw, bound);
    }

    @Override
    protected boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables,
                                              Set<ObjectVariable> bound) {
        return operand.freeForSubstitutionImpl(x,freeVariables,bound);
    }

    @Override
    protected void getFreeVariablesImpl(Set<ObjectVariable> allFreeVariables) {
        operand.getFreeVariablesImpl(allFreeVariables);
    }

    @Override
    protected void getPropositionalVariablesImpl(TreeSet<Predicate> set) {
        operand.getPropositionalVariablesImpl(set);
    }

    @Override
    protected boolean isTemplateImpl(Expression formToCompare, HashMap<String, Expression> variable) {
        return formToCompare instanceof Negation && this.operand.isTemplateImpl(((Negation) formToCompare).operand, variable);
    }

    @Override
    protected String getStringRepresentation() {
        return sign + (!(operand instanceof Predicate) && !operand.getClass().equals(this.getClass()) ? "(" : "") + operand.getStringRepresentation() +
                (!(operand instanceof Predicate) && !operand.getClass().equals(this.getClass()) ? ")" : "");
    }
}
