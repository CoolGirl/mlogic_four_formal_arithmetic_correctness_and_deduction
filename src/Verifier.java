import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Евгения on 06.09.14.
 */
public class Verifier {
    static List<Expression> axiomSchemes = Expression.parseFile("axiom schemes.txt");
    static List<Expression> axioms = Expression.parseFile("axioms.txt");
    private HashMap<Expression, List<Expression>> implicationListByRightOperand;
    private HashMap<Expression, Integer> expressionHashMap;

    /*static {
        try {
            br = new BufferedReader(new FileReader("axiom schemes.txt"));
            axiomSchemes = new ArrayList<>();
            axioms = new ArrayList<>();
            String currentExpression = br.readLine();
            //int currentAxiomScheme = 1;


            do {
                axiomSchemes.add(ExpressionParser.parse(currentExpression));
                axioms.add(ExpressionParser.parse(currentExpression));
                currentExpression = br.readLine();
            } while (currentExpression != null);
        } catch (IOException e) {
            System.out.println("Cannot read from file.");
        } catch (ExpressionParser.WrongExpressionException w) {
            System.out.println("Wrong expression.");
        }
    }     */

    public Verifier() {
        implicationListByRightOperand = new HashMap<>();
        expressionHashMap = new HashMap<>();
    }

    VerifyingResult tryModusPonens(Expression parsed) {
        if (implicationListByRightOperand.containsKey(parsed))
            for (Expression expression : implicationListByRightOperand.get(parsed)) {
                if (expressionHashMap.containsKey(((Implication) expression).left))
                    return new VerifyingResult(expressionHashMap.get(((Implication) expression).left),
                            expressionHashMap.get(expression));
            }
        return new VerifyingResult();
    }

    VerifyingResult tryUniversalQuantifierRule(Expression parsed) throws ProofException {
        if (!(parsed instanceof Implication) || !(((BinaryOperation) parsed).right instanceof UniversalQuantifier))
            return new VerifyingResult();
        Implication b = (Implication) parsed;
        UniversalQuantifier q = (UniversalQuantifier) b.right;
        Expression e = new Implication(b.left, q.getExpression());
        if (expressionHashMap.containsKey(e)) {
            if (!b.left.getFreeVariables().contains(q.getObjectVariable()))
                return new VerifyingResult(
                        VerifyingResult.PredicateRules.UNIVERSAL_QUANTIFIER_RULE, expressionHashMap.get(b));
            throw new ProofException("переменная " + q.getObjectVariable() + " входит свободно в формулу " + b.left);
        }
        return new VerifyingResult();
    }

    VerifyingResult tryExistentialQuntifierRule(Expression parsed) throws ProofException {
        if (!(parsed instanceof Implication) || !(((BinaryOperation) parsed).left instanceof ExistentialQuantifier))
            return new VerifyingResult();
        BinaryOperation b = (BinaryOperation) parsed;
        Quantifier q = (Quantifier) b.left;
        Expression e = new Implication(q.getExpression(), b.right);
        if (expressionHashMap.containsKey(e)) {
            if (!b.right.getFreeVariables().contains(q.getObjectVariable()))
                return new VerifyingResult(
                        VerifyingResult.PredicateRules.EXISTENTIAL_QUANTIFIER_RULE, expressionHashMap.get(b));
            throw new ProofException("переменная " + q.getObjectVariable() + " входит свободно в формулу " + b.right);
        }
        return new VerifyingResult();
    }

    VerifyingResult verifyExpression(Expression parsed, int expressionNumber) throws ProofException {
        VerifyingResult verifyingResult = tryModusPonens(parsed);
        if (parsed instanceof Implication)
            if (implicationListByRightOperand.containsKey(((Implication) parsed).right))
                implicationListByRightOperand.get(((Implication) parsed).right).add(parsed);
            else {
                List<Expression> auxList = new ArrayList<>();
                auxList.add(parsed);
                implicationListByRightOperand.put(((Implication) parsed).right, auxList);
            }
        if (!expressionHashMap.containsKey(parsed))
            expressionHashMap.put(parsed, expressionNumber);
        if (!verifyingResult.isVerifyingCorrect())
        if (isAxiomOrAxiomScheme(parsed))
            return new VerifyingResult(0);
        if (!verifyingResult.isVerifyingCorrect()) {
            verifyingResult = tryUniversalQuantifierRule(parsed);
        }
        if (!verifyingResult.isVerifyingCorrect()) {
            verifyingResult = tryExistentialQuntifierRule(parsed);
        }
        return verifyingResult;
    }


    public static boolean isAxiomOrAxiomScheme(Expression parsed) throws ProofException {
        for (Expression expression : axiomSchemes) {
            if (expression.isTemplateFor(parsed))
                return true;
        }
        for (Expression expression : axioms) {
            if (expression.equals(parsed))
                return true;
        }
        return parsed instanceof Implication && (isAxiomEleven(parsed) || isAxiomTwelve(parsed) || isAxiomInduction(parsed));
    }

    private static boolean isAxiomEleven(Expression parsed) throws ProofException{
        if (((Implication) parsed).left instanceof UniversalQuantifier) {
            Implication implication = (Implication) parsed;
            Quantifier quantifier = (Quantifier) (implication.left);
            Term t = quantifier.getExpression().isOneVariableTemplate(quantifier.getObjectVariable(),
                    implication.right);
            if (t!=null) {
                if (quantifier.getExpression().freeForSubstitution(quantifier.getObjectVariable(), t))
                    return true;
                throw new ProofException("Терм " + t + " не свободен для подстановки в формулу " + quantifier.getExpression()
                        + " вместо переменной " + quantifier.getObjectVariable());
            }
        }
        return false;
    }

    private static boolean isAxiomTwelve(Expression parsed) throws ProofException {
        if (((Implication) parsed).right instanceof ExistentialQuantifier) {
            Implication implication = (Implication) parsed;
            Quantifier quantifier = (Quantifier) (implication.right);
            Term t = quantifier.getExpression().isOneVariableTemplate(
                    quantifier.getObjectVariable(), implication.left);
            if (t!=null) {
                if (quantifier.getExpression().freeForSubstitution(quantifier.getObjectVariable(), t))
                    return true;
                throw new ProofException("Терм " + t + " не свободен для подстановки в формулу " + quantifier.getExpression()
                        + " вместо переменной " + quantifier.getObjectVariable());
            }
        }
        return false;
    }

    private static boolean isAxiomInduction(Expression parsed) {


        if (((Implication) parsed).left instanceof Conjunction) {
            Conjunction conjunction = (Conjunction) ((Implication) parsed).left;
            if (conjunction.right instanceof UniversalQuantifier) {
                UniversalQuantifier universalQuantifier = (UniversalQuantifier) conjunction.right;
                ObjectVariable substVariable = universalQuantifier.getObjectVariable();
                Expression psi = ((Implication) parsed).right;
                if (psi.getFreeVariables().contains(substVariable)) {
                    Term z = psi.isOneVariableTemplate(substVariable, conjunction.left);
                    if (z != null && z.equals(ObjectFunction.ZERO)) {
                        Expression quantifiedExpression = universalQuantifier.getExpression();
                        if (quantifiedExpression instanceof Implication) {
                            if (((Implication) quantifiedExpression).left.equals(psi)) {
                                Term i = psi.isOneVariableTemplate(substVariable, ((Implication) quantifiedExpression).right);
                                if (i != null && i.equals(new Increment(substVariable)))
                                    return true;
                            }

                        }
                    }
                }
            }
        }
        return false;
    }

}
