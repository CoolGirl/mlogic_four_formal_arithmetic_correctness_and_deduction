import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Евгения on 06.09.14.
 */
public abstract class BinaryOperation extends Expression {
    protected String sign;
    protected Expression left, right;

    @Override
    protected boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables, Set<ObjectVariable> bound) {
        return
                left.freeForSubstitutionImpl(x, freeVariables, bound)
                        &&
                        right.freeForSubstitutionImpl(x, freeVariables, bound);
    }

    static HashMap<String, Integer> priorityOperation;

    static {
        priorityOperation = new HashMap<>();
        priorityOperation.put("&", 1);
        priorityOperation.put("|", 2);
        priorityOperation.put("->", 3);
    }

    @Override
    protected void getFreeVariablesImpl(Set<ObjectVariable> allFreeVariables) {
        left.getFreeVariablesImpl(allFreeVariables);
        right.getFreeVariablesImpl(allFreeVariables);
    }

    @Override
    protected boolean isOneVariableTemplateImpl(ObjectVariable replaceWhat, Expression with,
                                                SubstitutionWrapper sw, HashSet<ObjectVariable> bound) {
        return this.getClass() == with.getClass() && left.isOneVariableTemplateImpl(replaceWhat,
                ((BinaryOperation) with).left, sw, bound) && right.isOneVariableTemplateImpl(replaceWhat,
                ((BinaryOperation) with).right, sw, bound);
    }


    @Override
    protected void getPropositionalVariablesImpl(TreeSet<Predicate> set) {
        left.getPropositionalVariablesImpl(set);
        right.getPropositionalVariablesImpl(set);
    }

    protected boolean isThisPriorityHigher(String signOne, String signOther) {
        return priorityOperation.get(signOne) < priorityOperation.get(signOther);
    }

    public BinaryOperation(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public abstract String getSign();


    @Override
    protected boolean isTemplateImpl(Expression formToCompare, HashMap<String, Expression> variable) {
        return ((formToCompare.getClass().equals(this.getClass())
                && this.left.isTemplateImpl(((BinaryOperation) formToCompare).left, variable)
                && this.right.isTemplateImpl(((BinaryOperation) formToCompare).right, variable)));
    }

    @Override
    protected abstract Expression substituteImpl(HashMap<Predicate, Expression> substByVarName);

    @Override
    protected String getStringRepresentation() {
        boolean leftBrackets = false, rightBrackets = false;
        if (left instanceof BinaryOperation && isThisPriorityHigher(sign, ((BinaryOperation) left).getSign()))
            leftBrackets = true;
        if (right instanceof BinaryOperation
                && (isThisPriorityHigher(sign, ((BinaryOperation) right).getSign())) || right.getClass().equals(getClass()))
            rightBrackets = true;
        return ((leftBrackets ? "(" : "")
                + left.toString()
                + (leftBrackets ? ")" : "")
                + sign
                + (rightBrackets ? "(" : "")
                + right.toString()
                + (rightBrackets ? ")" : ""));
    }
}
