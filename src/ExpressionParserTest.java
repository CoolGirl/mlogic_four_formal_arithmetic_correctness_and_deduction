import junit.framework.TestCase;

public class ExpressionParserTest extends TestCase {

    public void testTryParseExpression() throws Exception {
        String test1 = "@x (0=0)";
        assertEquals(test1.replaceAll("\\s+", ""), ExpressionParser.parse(test1).toString());
        String test2 = "P(f(x,y,z))";
        assertEquals(test2, ExpressionParser.parse(test2).toString().replaceAll("\\s+", ""));
        String test3 = "a=b'&@y(P(y))->y''=y'''''''''''";
        assertEquals(test3, ExpressionParser.parse(test3).toString().replaceAll("\\s+", ""));
        String test4 = "P";
        assertEquals(test4, ExpressionParser.parse(test4).toString().replaceAll("\\s+", ""));
        String test5 = "A123(x+((0*0)*(y'+x''')))->!P(x,y)|f(a0)=g(a2353,a)";
        assertEquals(test5, ExpressionParser.parse(test5).toString().replaceAll("\\s+", ""));
        String test6 = "(((A->A)->A)->A)&(B&(C&D))";
        assertEquals(test6, ExpressionParser.parse(test6).toString().replaceAll("\\s+", ""));
        String test7 = "f(f(f(g)))=0";
        assertEquals(test7, ExpressionParser.parse(test7).toString().replaceAll("\\s+", ""));
        String test8 = "?x010(P(x,y))";
        assertEquals(test8, ExpressionParser.parse(test8).toString().replaceAll("\\s+", ""));
        String test9 = "@x(?y(P(y)))";
        assertEquals(test9, ExpressionParser.parse(test9).toString().replaceAll("\\s+", ""));
        String test10 = "@x(?x(@x(0=0)))";
        assertEquals(test10, ExpressionParser.parse(test10).toString().replaceAll("\\s+", ""));

    }

    public void testCorrectness() throws Exception {
        Expression test1 = ExpressionParser.parse("P(x)");
        Expression test2 = ExpressionParser.parse("P(f(z))");
        assertNotNull(test1.isOneVariableTemplate(new ObjectVariable("x"), test2));
        assertNull(test1.isOneVariableTemplate(new ObjectVariable("y"), test2));
        assertNotNull(test1.isOneVariableTemplate(new ObjectVariable("x"), test1));
        Expression test3 = ExpressionParser.parse("@xP(x)");
        assertNotNull(test3.isOneVariableTemplate(new ObjectVariable("x"), test3));
        Expression test4 = ExpressionParser.parse("@x?yf(g(p(x,y)))'=0");
        assertNull(test4.isOneVariableTemplate(new ObjectVariable("y"), ExpressionParser.parse(
                "@x?pf(g(p(x,p)))'=0")));
        assertNull(test4.isOneVariableTemplate(new ObjectVariable("y"), ExpressionParser.parse(
                "@x?xf(g(p(x,x)))'=0")));
        Expression test5 = ExpressionParser.parse("@xP(x)->P(x)");
        assertNotNull(test5.isOneVariableTemplate(new ObjectVariable("x"),
                ExpressionParser.parse("@xP(x)->P(y0)")));
        assertNull(ExpressionParser.parse("?x@xP(x, y)").isOneVariableTemplate(new ObjectVariable("x"),
                ExpressionParser.parse("@xP(y)->P(y0)")));
        assertNotNull(ExpressionParser.parse("A(x)&B(x, f(x))->A(x, f(x), f(f(x)))").isOneVariableTemplate(
                new ObjectVariable("x"), ExpressionParser.parse("A(f(z)) & B(f(z), f(f(z))) -> A(f(z), f(f(z)), f(f(f(z))))")));
        assertNotNull(ExpressionParser.parse("A(x)&B(x, f(x))->A(x, f(x), f(f(x)))->@y(y=x)&@x(x=y)").isOneVariableTemplate(
                new ObjectVariable("x"), ExpressionParser.parse("A(f(z)) & B(f(z), f(f(z))) -> A(f(z), f(f(z)), f(f(f(z)))) -> @y(y=f(z))&@x(x=y)")));
    }


    public void testVerification() throws Exception {
        Verifier verifier = new Verifier();
        VerifyingResult verifyingResult = verifier.verifyExpression(ExpressionParser.parse("A->A->A"), 0);
        assertTrue(verifyingResult.isVerifyingCorrect());
        VerifyingResult verifyingResult1 = verifier.verifyExpression(ExpressionParser.parse("@x(P(x))->P(a)"), 0);
        assertTrue(verifyingResult1.isVerifyingCorrect());
    }

    public void testInductionAxiom() throws ExpressionParser.WrongExpressionException, ProofException {
        Verifier verifier = new Verifier();
        VerifyingResult result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "P(0)&@x(P(x) -> P(x')) -> P(x)"), 0);
        assertTrue(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(0))&@x(P(x)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertTrue(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(x)&!Q(y))&@x(P(x)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(x))&@x(P(x)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(x))&@x(P(x)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(0))&@x(P(0)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(0))&@x(P&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(0))&@x(P(x)&!Q(x) -> P(x)&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(0))&?x(P(0)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "(P(0)&!Q(0))|@x(P(0)&!Q(x) -> P(x')&!Q(x')) -> P(x)&!Q(x)"), 1);
        assertFalse(result.isVerifyingCorrect());

        result = verifier.verifyExpression(
                ExpressionParser.parse(
                        "P(0)&?x(P(x)&?y(!(x=y)&P(y)))->P(x)"), 1);
        assertFalse(result.isVerifyingCorrect());
    }

}