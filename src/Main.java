import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by Евгения on 28.02.2015.
 */

public class Main {
    public static void main(String[] args) throws IOException, ExpressionParser.WrongExpressionException {
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        PrintWriter pw = new PrintWriter("output.txt", "UTF-8");
        String currentExpression = br.readLine();
        currentExpression = currentExpression.replaceAll("//s+", "");
        ArrayList<Expression> assumptions = new ArrayList<Expression>();
        String[] stringAssumptionAux = currentExpression.split("\\|-");
        String[] stringAssumption = stringAssumptionAux[0].split(",");
        for (int i = 0; i < stringAssumption.length; i++) {
            assumptions.add(ExpressionParser.parse(stringAssumption[i]));
            if (i != stringAssumption.length - 1)
                pw.print(assumptions.get(i).toString() + (i == stringAssumption.length - 2 ? "" : ", "));
        }
        pw.print("|-");
        Expression beta = ExpressionParser.parse(stringAssumptionAux[1]);
        pw.println(new Implication(assumptions.get(assumptions.size() - 1), beta));
        int stringNumber = 1;
        ArrayList<Expression> betaProof = new ArrayList<>();
        currentExpression = br.readLine();
        do {
            betaProof.add(ExpressionParser.parse(currentExpression));
            stringNumber++;
            currentExpression = br.readLine();
        }
        while (currentExpression != null);
        Verifier verifier = new Verifier();
        int i;
        for (i = 0; i < betaProof.size(); i++) {
            VerifyingResult verifyingResult;
            try {
                verifyingResult = verifier.verifyExpression(betaProof.get(i), stringNumber);
                if (!verifyingResult.isVerifyingCorrect()) {
                    if (assumptions.contains(betaProof.get(i)) || assumptions.get(assumptions.size() - 1).equals(betaProof.get(i)))
                        continue;
                    pw.print(verifyingResult.toString() + (i + 1) + " формула не является аксиомой, допущением или применением" +
                            "правила Modus Ponens.");
                    break;
                }
            } catch (ProofException e) {
                pw.println(new VerifyingResult().toString() + (i + 1) + " " + e.getMessage());
                break;
            }
        }
        ArrayList<Expression> deductionTheoremResult;
        if (i == betaProof.size())
            try {
                deductionTheoremResult = DeductionTheorem.apply(assumptions, betaProof);
                for (Expression aDeductionTheoremResult : deductionTheoremResult)
                    pw.println(aDeductionTheoremResult);
            } catch (ProofException e) {
                pw.println(e.getMessage());
                pw.close();
                return;
            }
        pw.close();
    }
}

