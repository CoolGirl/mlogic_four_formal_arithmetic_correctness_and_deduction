import java.util.HashSet;
import java.util.Set;

/**
 * Created by Евгения on 16.02.2015.
 */
public abstract class Term {
    protected String name;
    protected String stringRepresentation;

    public Term(String name){
        this.name = name;
    }

    public String toString() {
        if (stringRepresentation == null)
            stringRepresentation = getStringRepresentation();
        return stringRepresentation;
    }

    protected abstract void getFreeVariables(Set<ObjectVariable> allFreeVariables);

    protected abstract boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables,
                                                       Set<ObjectVariable> bound);

    public abstract boolean equalsWithObjectVariableSubstitution(ObjectVariable replaceWhat, Term
            expressionWithSubstitution, Expression.SubstitutionWrapper sw, HashSet<ObjectVariable> bound);

    protected abstract String getStringRepresentation();

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Term && toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
