import java.util.*;

/**
 * Created by Евгения on 16.02.2015.
 */
public class Quantifier extends Expression {

    private String sign;
    private ObjectVariable objectVariable;
    private Expression expression;

    protected Quantifier(String sign, ObjectVariable objectVariable, Expression expression) {
        this.sign = sign;
        this.objectVariable = objectVariable;
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public ObjectVariable getObjectVariable() {
        return objectVariable;
    }

    @Override
    protected boolean isTemplateImpl(Expression formToCompare, HashMap<String, Expression> variable) {
        return true;
        // this method is never called (because I haven't any axiom scheme with quantifiers)
    }

    @Override
    protected void getPropositionalVariablesImpl(TreeSet<Predicate> set) {
       // this method mustn't do something
    }


    @Override
    protected String getStringRepresentation() {
        return sign + objectVariable.toString() + "(" + expression.toString() + ")";
    }

    @Override
    protected Expression substituteImpl(HashMap<Predicate, Expression> substByVarName) {
        return null;
    }

    @Override
    public boolean isOneVariableTemplateImpl(ObjectVariable replaceWhat, Expression with,
                                             SubstitutionWrapper sw, HashSet<ObjectVariable> bound) {
        boolean wasBound = false;
        if (getClass() != with.getClass() || !objectVariable.equals(((Quantifier) with).objectVariable))
            return false;
        else {
            if (!bound.contains(objectVariable))
                bound.add(objectVariable);
            else
                wasBound = true;
            boolean result = expression.isOneVariableTemplateImpl(replaceWhat, ((Quantifier) with).expression,
                    sw, bound);
            if (!wasBound)
                bound.remove(objectVariable);
            return result;
        }
    }

    @Override
    protected boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables, Set<ObjectVariable> bound) {
        boolean wasBound = bound.contains(objectVariable);
        if (!wasBound)
            bound.add(objectVariable);
        boolean isFree = expression.freeForSubstitutionImpl(x,freeVariables,bound);
        if (!wasBound)
            bound.remove(objectVariable);
        return isFree;
    }

    @Override
    protected void getFreeVariablesImpl(Set<ObjectVariable> allFreeVariables) {
         boolean wasVisited =allFreeVariables.contains(objectVariable);
         expression.getFreeVariablesImpl(allFreeVariables);
        if (!wasVisited)
            allFreeVariables.remove(objectVariable);
    }
}

class ExistentialQuantifier extends Quantifier {

    protected ExistentialQuantifier(ObjectVariable objectVariable, Expression expression) {
        super("?", objectVariable, expression);
    }
}

class UniversalQuantifier extends Quantifier {


    protected UniversalQuantifier(ObjectVariable objectVariable, Expression expression) {
        super("@", objectVariable, expression);
    }
}


