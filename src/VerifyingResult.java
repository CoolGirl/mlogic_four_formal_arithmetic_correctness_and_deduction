/**
 * Created by Евгения on 07.09.14.
 */
public class VerifyingResult {
    private String result;
    private boolean correctVerifying = true;
    private boolean modusPonens;
    private boolean existentialQuantifierRule;
    private boolean universalQuantifierRule;
    private int expressionNumberForRule;
    private int axiomOrFirstPonensArg;
    private int secondPonensArg;

    public static enum PredicateRules{
        UNIVERSAL_QUANTIFIER_RULE,
        EXISTENTIAL_QUANTIFIER_RULE
    }

    public VerifyingResult() {
        result = "Вывод некорректен начиная с формулы номер ";
        correctVerifying = false;
    }

    public VerifyingResult(PredicateRules rule, int expressionNumber){
        if (rule.equals(PredicateRules.EXISTENTIAL_QUANTIFIER_RULE))
            existentialQuantifierRule = true;
        else
            universalQuantifierRule = true;
        expressionNumberForRule = expressionNumber;
    }

    public VerifyingResult(int axiomNumber) {
        modusPonens = false;
        result = "Схема аксиом " + axiomNumber;
        axiomOrFirstPonensArg = axiomNumber;
    }

    public VerifyingResult(int expressionNumber, int otherExpressionNumber) {
        modusPonens = true;
        result = "Modus ponens " + expressionNumber + ", " + otherExpressionNumber;
        axiomOrFirstPonensArg = expressionNumber;
        secondPonensArg = otherExpressionNumber;
    }

    public boolean isVerifyingCorrect(){
         return correctVerifying;
    }
    @Override
    public String toString() {
        return result;
    }

    public boolean isModusPonens(){
        return modusPonens;
    }

    public boolean isExistentialQuantifierRule(){
        return existentialQuantifierRule;
    }

    public boolean isUniversalQuantifierRule(){
        return universalQuantifierRule;
    }

    public int getAxiomOrFirstPonensArg(){
        return axiomOrFirstPonensArg;
    }

    public int getSecondPonensArg(){
        return secondPonensArg;
    }
}
