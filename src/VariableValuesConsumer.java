import java.util.HashMap;

/**
 * Created by Евгения on 09.02.2015.
 */
public interface VariableValuesConsumer {

    void consumeVariableValues(HashMap<Predicate, Boolean> values);
}
