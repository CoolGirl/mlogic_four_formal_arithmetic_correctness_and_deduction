import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Евгения on 04.09.14.
 */
public abstract class Expression implements Comparable<Expression> {

    String stringExpression = null;

    boolean isTemplateFor(Expression formToCompare) {
        return isTemplateImpl(formToCompare, new HashMap<String, Expression>());
    }

    abstract protected boolean isTemplateImpl(Expression formToCompare, HashMap<String, Expression> variable);

    public TreeSet<Predicate> getPropositionalVariables() {
        TreeSet<Predicate> set = new TreeSet<>();
        getPropositionalVariablesImpl(set);
        return set;
    }

    public void forAllVariableValues(VariableValuesConsumer consumer) {
        TreeSet<Predicate> allPropositionalVariables = getPropositionalVariables();
        int n = allPropositionalVariables.size();
        for (int i = 0; i < 1 << n; i++) {
            int position = n - 1;
            HashMap<Predicate, Boolean> values = new HashMap<>();
            for (Predicate variable : allPropositionalVariables) {
                values.put(variable, ((i >> position) & 1) == 1);
                position--;
            }
            consumer.consumeVariableValues(values);
        }
    }

    protected abstract void getPropositionalVariablesImpl(TreeSet<Predicate> set);


    public static List<Expression> parseFile(String fileName) {
        String currentLine;
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            return null;
        }
        List<Expression> proofList = new ArrayList<>();
        int counter = 1;
        try {
            while ((currentLine = br.readLine()) != null) {
                proofList.add(ExpressionParser.parse(currentLine));
                ++counter;
            }
        } catch (ExpressionParser.WrongExpressionException exception) {
            System.out.println(exception.getMessage() + " (line " + counter + ")");
        } catch (IOException e) {
            return null;
        }
        return proofList;
    }

    public static List<Expression> substituteAll(List<Expression> proofList, HashMap<Predicate, Expression> substMap) {
        List<Expression> result = new ArrayList<>();
        for (Expression expression : proofList) {
            result.add(expression.substitute(substMap));
        }
        return result;
    }


    protected abstract String getStringRepresentation();

    protected abstract Expression substituteImpl(HashMap<Predicate, Expression> substByVarName);

    public Expression substitute(HashMap<Predicate, Expression> substByVarName) {
        Expression result = substituteImpl(substByVarName);
        return result != null ? result : this;
    }

    public String toString() {
        if (stringExpression == null)
            stringExpression = getStringRepresentation();
        return stringExpression;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Expression && this.toString().equals(obj.toString());
    }

    public Term isOneVariableTemplate(ObjectVariable replaceWhat, Expression
            expressionWithSubstitution) {
        SubstitutionWrapper sw = new SubstitutionWrapper(null);
        if (isOneVariableTemplateImpl(replaceWhat, expressionWithSubstitution, sw,
                new HashSet<ObjectVariable>()))
            return sw.substitution!=null ? sw.substitution : replaceWhat;
        return null;

    }

    protected static class SubstitutionWrapper {
        Term substitution;
        public SubstitutionWrapper(Term substitution) {
            this.substitution = substitution;
        }
    }

    protected abstract boolean isOneVariableTemplateImpl(ObjectVariable replaceWhat, Expression with,
                                                         SubstitutionWrapper sw,
                                                         HashSet<ObjectVariable> bound);

    public Set<ObjectVariable> getFreeVariables() {
        HashSet<ObjectVariable> result = new HashSet<>();
        getFreeVariablesImpl(result);
        return result;
    }

    public boolean freeForSubstitution(ObjectVariable x, Term teta) {
        HashSet<ObjectVariable> tetaVariables = new HashSet<>();
        teta.getFreeVariables(tetaVariables);
        return freeForSubstitutionImpl(x, tetaVariables, new HashSet<ObjectVariable>());

    }

    protected abstract boolean freeForSubstitutionImpl(ObjectVariable x, Set<ObjectVariable> freeVariables,
                                                       Set<ObjectVariable> bound);

    protected abstract void getFreeVariablesImpl(Set<ObjectVariable> allFreeVariables);

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public int compareTo(Expression o) {
        return toString().compareTo(o.toString());
    }
}
