import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Евгения on 04.09.14.
 */
public class ExpressionParser {

    private int currentIndex;
    private String expression;


    static class WrongExpressionException extends Exception {
        WrongExpressionException(ExpressionParser parser) {
            super("Invalid expression "
                    + parser.expression
                    + " at position "
                    + parser.currentIndex);
        }
    }

    private ExpressionParser(String userExpression) {
        currentIndex = 0;
        expression = userExpression;
    }

    public static Expression parse(String userExpression) throws WrongExpressionException {
        ExpressionParser parser = new ExpressionParser(userExpression.replaceAll("\\s+", ""));
        Expression result = parser.parseExpression();
        if (parser.currentIndex != parser.expression.length())
            throw new WrongExpressionException(parser);
        return result;
    }

    private Expression parseExpression() throws WrongExpressionException {
        Expression parsedExpression = parseDisjunction();
        if (tryConsume("->")) {
            return new Implication(parsedExpression, parseExpression());
        }
        return parsedExpression;
    }

    private Expression parseDisjunction() throws WrongExpressionException {
        Expression parsedExpression = parseConjunction();
        while (tryConsume("|")) {
            parsedExpression = new Disjunction(parsedExpression, parseConjunction());
        }
        return parsedExpression;
    }

    private boolean tryConsume(String symbol) {
        for (int i = 0; i < symbol.length() && currentIndex + i < expression.length(); ++i) {
            if (symbol.charAt(i) != expression.charAt(currentIndex + i))
                return false;
            if (i == symbol.length() - 1) {
                currentIndex += symbol.length();
                return true;
            }
        }
        return false;
    }

    private Expression parseConjunction() throws WrongExpressionException {
        Expression parsedExpression = parseUnary();
        while (tryConsume("&")) {
            parsedExpression = new Conjunction(parsedExpression, parseUnary());
        }
        return parsedExpression;
    }

    private List<Term> tryParseArgs() throws WrongExpressionException {
        if (!tryConsume("("))
            return null;
        List<Term> result = new ArrayList<>();
        do {
            result.add(parseTerm());
        } while (tryConsume(","));
        if (!tryConsume(")"))
            throw new WrongExpressionException(this);
        return result;
    }

    private Term parseTerm() throws WrongExpressionException {
        Term parsedExpression = parseSummand();
        while (tryConsume("+")) {
            parsedExpression = new Addition(parsedExpression, parseSummand());
        }
        return parsedExpression;
    }

    private Term parseSummand() throws WrongExpressionException {
        Term parsedExpression = parseMultiplier();
        while (tryConsume("*")) {
            parsedExpression = new Multiplication(parsedExpression, parseMultiplier());
        }
        return parsedExpression;
    }

    private Term parseMultiplier() throws WrongExpressionException {
        Term parsedExpression;
        String name = tryConsumeName('a', 'z');
        if (name != null) {
            List<Term> args = tryParseArgs();
            if (args != null)
                parsedExpression = new ObjectFunction(name, args);
            else
                parsedExpression = new ObjectVariable(name);
        } else
            parsedExpression = parseObjectVariable();
        if (parsedExpression == null) {
            if (tryConsume("(")) {
                parsedExpression = parseTerm();
                if (!tryConsume(")"))
                    throw new WrongExpressionException(this);
            } else if (tryConsume("0"))
                parsedExpression = ObjectFunction.ZERO;
        }
        while (tryConsume("'"))
            parsedExpression = new Increment(parsedExpression);
        return parsedExpression;
    }

    private boolean isInRange(char letter, char first, char last) {
        return (letter >= first && letter <= last);
    }

    private Expression parseUnary() throws WrongExpressionException {
        Expression parsedExpression = null;
        if (currentIndex < expression.length()) {
            if (tryConsume("!")) {
                parsedExpression = new Negation(parseUnary());
            } else if (tryConsume("(")) {
                int savedPosition = currentIndex - 1;
                try {
                    parsedExpression = parseExpression();
                } catch (WrongExpressionException ignored) { }
                if (parsedExpression != null) {
                    if (!tryConsume(")"))
                        throw new WrongExpressionException(this);
                }
                else
                    currentIndex = savedPosition;
            } else if (tryConsume("@")) {
                return new UniversalQuantifier(parseObjectVariable(), parseUnary());
            } else if (tryConsume("?")) {
                return new ExistentialQuantifier(parseObjectVariable(), parseUnary());
            } else {
                String name = tryConsumeName('A', 'Z');
                if (name != null)
                    parsedExpression = new Predicate(name, tryParseArgs());

            }
            if (parsedExpression == null) {
                Term firstTerm = parseTerm();
                if (!tryConsume("="))
                    throw new WrongExpressionException(this);
                Term secondTerm = parseTerm();
                if (secondTerm == null)
                    throw new WrongExpressionException(this);
                else
                    parsedExpression = new EqualsPredicate(firstTerm, secondTerm);
            }
        }
        if (parsedExpression == null)
            throw new WrongExpressionException(this);
        return parsedExpression;
    }

    private ObjectVariable parseObjectVariable() {
        String name = tryConsumeName('a', 'z');
        return name == null ? null : new ObjectVariable(name);
    }

    private String tryConsumeName(char firstLetterInRange, char lastLetterInRange) {
        int begin = currentIndex;
        if (isInRange(expression.charAt(currentIndex), firstLetterInRange, lastLetterInRange)) {
            currentIndex++;
            while (currentIndex < expression.length() && Character.isDigit(expression.charAt(currentIndex)))
                currentIndex++;
            return (expression.substring(begin, currentIndex));
        }
        return null;
    }

}
